package com.venus.java;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sun.misc.Regexp;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;

import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;

public class MainClass {
    public static void main(String[] args) {
        BytePointer outText;
        ArrayList<String> lines = new ArrayList<>();

        tesseract.TessBaseAPI api = new tesseract.TessBaseAPI();
        // Initialize tesseract-ocr with English, without specifying tessdata path
        if (api.Init("/usr/share/tesseract-ocr/tessdata", "fra") != 0) {
            System.err.println("Could not initialize tesseract.");
            System.exit(1);
        }

        final String subImagePath = "subImage.png";
        lept.PIX image = null;
        int[] breakPoints = new int[]{0, 75, 100, 125, 150};
        try {
            BufferedImage bimage = ImageIO.read(new File(args.length > 0 ? args[0] : "image_pv.png"));
            image = pixRead(subImagePath);

            for (int i = 0; i < breakPoints.length - 1; i++) {
                BufferedImage subImage = bimage.getSubimage(
                    0,
                    breakPoints[i],
                    bimage.getWidth(),
                    breakPoints[i + 1] - breakPoints[i]
                );
                ImageIO.write(subImage, "png", new File(subImagePath));
                image = pixRead(subImagePath);
                api.SetImage(image);
                String s = api.GetUTF8Text().getString();
                lines.add(s.replace("\n", ""));
            }

            int suffragesCount = 25;
            for (int i = 0; i < 5; i++) {
                BufferedImage subImage = bimage.getSubimage(
                        0,
                        205 + suffragesCount * i + (i == 0 ? 0 : 5),
                        bimage.getWidth(),
                        suffragesCount * (i + 1) - suffragesCount * i
                );
                ImageIO.write(subImage, "png", new File(subImagePath));
                image = pixRead(subImagePath);
                api.SetImage(image);
                String s = api.GetUTF8Text().getString();
                lines.add(s.replace("\n", ""));
                System.out.println(lines.get(lines.size() - 1));
            }

            // data processing
            JSONObject data = new JSONObject();
            JSONArray suffrages = new JSONArray();
            String bureau = lines.get(1);
            data.put("bureau_vote", bureau.substring(bureau.indexOf(":") + 2));

            try {
                String inscrits = lines.get(2);
                data.put(
                    "nombre_inscrits",
                    Integer.valueOf(
                        inscrits.substring(
                            inscrits.indexOf(":") + 2
                        )
                    )
                );
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            try {
                String participants = lines.get(3);
                data.put(
                    "nombre_participants",
                    Integer.valueOf(
                        participants.substring(
                            participants.indexOf(":") + 2
                        )
                    )
                );
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            for (int i = 4; i < lines.size(); i++) {
                String line = lines.get(i);
                // soustraction des parasites
                line = line.replaceAll("[\\|\\[\\]]", "");
                String parti, voix, percent;
                String[] splits = line.split(" ");
                percent = splits[splits.length - 2]; // avant dernier
                voix = splits[splits.length - 3]; // avant avant dernier
                // on recoupere tout ce qui est avant l'avant avant dernier element
                // pour constituer le nom du parti
                parti = String.join(" ", Arrays.copyOfRange(splits, 0, splits.length - 3));
                JSONObject suffItem = new JSONObject();
                suffItem.put("parti", parti);
                suffItem.put("voix", Double.valueOf(voix));
                suffItem.put("percent", Double.valueOf(percent));
                suffrages.put(suffItem);
            }
            data.put("suffrages", suffrages);

            // returning result
            System.out.println(data.toString());
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            api.End();
            if (image != null)
                pixDestroy(image);
        }
    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static String readFile(FileInputStream fis) {
        FileChannel fc;
        ByteBuffer bb;

        try {
            fc = fis.getChannel();
            bb = ByteBuffer.allocate((int) fc.size());
            fc.read(bb);
            bb.flip();
            return new String(bb.array());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "erreur de lecture";
    }
}
